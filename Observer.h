//=================================
// include guard

#ifndef __OBSERVER_H_INCLUDED__   
#define __OBSERVER_H_INCLUDED__  

//=================================
// forward declared dependencies
class Observable;

//=================================
// included dependencies
#include <iostream>

//=================================
// the actual class

class Observer
{
public:
	Observer(){};
	virtual void notify() = 0;
	virtual ~Observer() { };
	void setObserving(Observable* observable){ m_observable = observable; };
protected:
	Observable* m_observable;
};

#endif

