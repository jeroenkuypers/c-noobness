#pragma once
#include "Observer.h"
#include "Observable.h"
class Log :
	public Observer
{
public:
	Log();
	void notify();
	~Log();
};

