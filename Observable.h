#pragma once
#include "Observer.h"
#include <vector>
#include <exception>
#include <iostream>

using namespace std;

struct ObserverFound{};

class Observable
{
public:
	Observable();	
	void addObserver(Observer *o);
	void unregisterObserver(Observer *o);
	virtual int getSize();
	virtual void notifyObservers() = 0;
	virtual ~Observable();
protected:
	std::vector<Observer*> m_observers;
};



