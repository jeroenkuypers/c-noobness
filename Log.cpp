#include "stdafx.h"
#include "Log.h"
#include <iostream>

using namespace std;

Log::Log()
{
}

void Log::notify()
{
	if (m_observable)
		cout << "New size is: " << m_observable->getSize() << endl;
	else
		cout << "ERROR, no observables" << endl;
}


Log::~Log()
{
	if (m_observable)
		m_observable->unregisterObserver(this);
}
