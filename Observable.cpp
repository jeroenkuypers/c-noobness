#include "stdafx.h"
#include "Observable.h"

Observable::Observable()
{
}

void Observable::notifyObservers()
{}

void Observable::addObserver(Observer *o)
{
	m_observers.push_back(o);
	o->setObserving(this);
}

void Observable::unregisterObserver(Observer *o)
{
	vector<Observer*>::iterator it = m_observers.begin();
	
	try{
		while (it < m_observers.end())
		{
			if (*it == o)
				throw ObserverFound();

			it += 1;
		}

		cout << "Warning failed to unregister Observer. " << endl;
	}
	catch (ObserverFound p)
	{
		cout << "Unregistering observer." << endl;
		m_observers.erase(it);
		cout << "?" << endl;
	}
}

Observable::~Observable()
{
}
