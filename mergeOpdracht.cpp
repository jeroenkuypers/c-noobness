
#include "stdafx.h"

#include "mergeOpdracht.h"

#include <algorithm> 
#include <string>

using namespace std;

// Start with two lists of names, both stored as std::vector<std::string>
// Merge the lists, but eliminate duplicate names.
// Print the elements of the merged list.

void copyUniqueNames(vector<string>& goalVec, const vector<string>& namesVec)
{
	
	for (auto &curName : namesVec)
	{
		vector<string>::iterator it = find(goalVec.begin(), goalVec.end(), curName);
		if (it == goalVec.end())
			goalVec.push_back(curName);
	}
}


void printNames(const vector<string>& names)
{
	int i = 0;
	for (string s : names)
		cout << ++i << ". " << s << endl;
}

bool myUniquefunction(string& str1, string& str2)
{
	return str1.compare(str2) == 0;
}

vector<string> mergeVecs(vector<string>& names1, vector<string>& names2)
{
	std::vector<string> result = vector<string>(names1.size() + names2.size());
	cout << result.capacity() << endl;
	
	copyUniqueNames(result, names1);
	copyUniqueNames(result, names2);

	//result = std::merge(names1, names2);

	std::sort(names1.begin(), names1.end());
	std::sort(names2.begin(), names2.end());
	std::merge(names1.begin(), names1.end(), names2.begin(), names2.end(), result.begin());

	//ALTERNATE:
	//vector<string> result2 = vector<string>(result);
	//vector<string>::iterator it2 = unique(result2.begin(), result2.end(), myUniquefunction);
	//result2.resize(distance(result2.begin(), it2));

	vector<string>::iterator it = std::unique(result.begin(), result.end());   
	result.resize(distance(result.begin(), it));
		
	return result;
}


//ok ok, geen nette class. Maar even een hack zodat ik dit bestand ook kon runnen in mijn huidige VC++ project.
mergeOpdracht::mergeOpdracht()
{}

void mergeOpdracht::runMe()
{
	
	vector<std::string> names1 = { "Jeroen", "Johnny", "Laura", "Chefke", "Bonnie" };	
	vector<std::string> names2 = { "Peter", "Chefke", "Peter", "Franky", "Captain D. Luffy", "Bonnie" };
	vector<std::string> result = mergeVecs(names1, names2);
	printNames(result);

	cin.ignore();
}

mergeOpdracht::~mergeOpdracht()
{}

