#pragma once
#include "Observer.h"
#include "Observable.h"

class SquareViewer :
	public Observer
{
public:
	SquareViewer();
	void notify();
	~SquareViewer();
};

