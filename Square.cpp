#include "stdafx.h"
#include "Square.h"
#include <iostream>

Square::Square(int size = 1) : m_size(size)
{
}

void Square::notifyObservers()
{
	for (Observer *o : m_observers)
	{
		o->notify();
	}
}

Square::~Square()
{

}
