// ConsoleApplication6.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Observer.h"
#include "Square.h"
#include "Log.h"
#include "SquareViewer.h"
#include "mergeOpdracht.h"
#include <iostream>

using namespace std;

int main()
{
	//mergeOpdracht merge = mergeOpdracht();
	//merge.runMe();

	Square s = Square(9);
	Log log = Log();
	SquareViewer viewer = SquareViewer();
	
	s.addObserver(&log);
	s.addObserver(&viewer);	
	s.notifyObservers();

	cin.ignore();
	return 0;
}

