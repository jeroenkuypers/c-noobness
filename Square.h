//=================================
// include guard
#ifndef _SQUARE_H_INCLUDED_
#define _SQUARE_H_INCLUDED_

//=================================
// forward declared dependencies

//=================================
// included dependencies

#include "Observable.h"

//=================================
// The actual class

class Square : public Observable
{
public:
	Square(int size);	
	void notifyObservers();
	int getSize() { return m_size; };
	~Square();
private:
	int m_size;
	
};

#endif

