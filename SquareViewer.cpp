﻿#include "stdafx.h"
#include "SquareViewer.h"
#include <iostream>
#include <string>


SquareViewer::SquareViewer()
{
}

//print some nice ASCII-art
void SquareViewer::notify()
{
	std::cout << endl << "==== Viewer ====" << endl << endl;

	if (!m_observable)
	{
		std::cout << "nothing to view" << endl;
		return;
	}
	
	int newSize = m_observable->getSize();

	if (newSize < 2)
		return;

	string top;
	string middle;
	const int  last = newSize - 1;
	
	top = "+";
	middle = "|";	

	for (int i = 1; i<(newSize - 1); ++i)
	{
		top += "-";
		middle += " ";		
	}

	top += "+"; 
	middle += "|";	
			
	cout << top << endl;
	for (int i = 1; i < (newSize - 1); ++i)
		cout << middle << endl;

	cout << top << endl;
}


SquareViewer::~SquareViewer()
{
	if (m_observable)
		m_observable->unregisterObserver(this);
}
